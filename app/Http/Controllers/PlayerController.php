<?php

namespace App\Http\Controllers;
use App\Player;
use App\Playergroup;
use DB;
use Illuminate\Http\Request;

class PlayerController extends Controller
{
//    public function __construct() {
//        $this->middleware('auth:admin');
//    }
    public function index() {
        $player          = Player::all();
        $data['player'] = $player ; 
        return view('player.index',$data);
    }
    public function setup($id = "") {
        $data = array();
        $data['button']         = "Add Player";
        $player_group           = Playergroup::where('club_id',\App\Club::loginClub())->get();
        $data['player_groups']  = $player_group;
        if($id != "") {
            $player                 = Player::where('id',$id)->first();
            $data['player']         = $player;
            $data['button']     = "Update Player";
        }
        return view('player.form',$data);
    }
    public function commit(Request $request) {
        $id = $request->input('player_id'); 
       if($id != "") {
            //return $request->all();
            $this->validate($request,[
               'player_name'    =>'required',
               'player_group'   =>'required',
            ]);
            $user = Player::find($id);
            $user->name         = $request->input('player_name');
            $user->club_id      = \App\Club::loginClub();
            $user->player_group = $request->input('player_group');
           
            if($request->hasfile('player_image'))
               {
                  $users = Player::where('id',$id)->first();
                  if(file_exists(public_path('img/player').'/'.$users->image)){
                        unlink(public_path('img/player').'/'.$users->image);
                   }
                  $images   = $request->file('player_image');
                  $names    = time().$images->getClientOriginalName();
                  $images->move(public_path().'/img/player/', $names);  
                  $user->image    = $names;
               }
            $user->save();
            
       } else { 
            $this->validate($request,[
               'player_name'    =>'required',
               'player_image'   =>'required|image|mimes:jpeg,jpg',
               'player_group'   =>'required',
            ]);

            $names = "";
            if($request->hasfile('player_image'))
               {
                  $images = $request->file('player_image');
                  $names = time().$images->getClientOriginalName();
                  $images->move(public_path().'/img/player/', $names);   
               }

             $crud = new Player([
                'name'          => $request->input('player_name'),
                'club_id'       => \App\Club::loginClub(),
                'player_group'  => $request->input('player_group'),
                'image'         => $names,
              ]);
             
              $crud->save();
       }
        return redirect('/player');
    }
    public function delete($id){
        $player = Player::where('id',$id)->first();
        //return $users; 
        if(file_exists(public_path('img/player').'/'.$player->image)){
              unlink(public_path('img/player').'/'.$player->image);
         }
        $affectedRows = Player::where('id','=',$id);
        $affectedRows->delete();
        return redirect('/player');
    }
}
