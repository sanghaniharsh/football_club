<?php

namespace App\Http\Controllers;
use App\Playergroup;
use DB;
use Illuminate\Http\Request;

class PlayergroupController extends Controller
{

    public function index() {
        $data['playergroup']    = Playergroup::leftJoin('team', 'team.id', '=', 'player_group.team_id')
                                ->select('player_group.*','team.name as team_name')
                                ->where('player_group.club_id','=',\App\Club::loginClub())
                                ->get();
        //return $clubs; 
        return view('playergroup.index',$data);
    }
    public function setup($id = "") {
        $data = array();
        $data['button']     = "Add Playergroup";
        if($id != "") {
            $data['playergroup']    = Playergroup::where('id',$id)->first();;
            $data['button']         = "Update Playergroup";
        }
        $data['teams'] = \App\Team::where('club_id',\App\Club::loginClub())->get();
        return view('playergroup.form',$data);
    }
    public function commit(Request $request) {
        $id = $request->input('playergroup_id'); 
       if($id != "") {
            $this->validate($request,[
                'name'      =>'required',
                'team_id'   =>'required'
            ]);
            $user = Playergroup::find($id);
            $user->name         = $request->input('name');
            $user->team_id      = $request->input('team_id');
            $user->club_id      = \App\Club::loginClub();
            $user->save();
            
       } else {
            $this->validate($request,[
               'name'       =>'required',
               'team_id'    =>'required',
            ]);

             $crud = new Playergroup([
                'name'          => $request->input('name'),
                'team_id'       => $request->input('team_id'),
                'club_id'       => \App\Club::loginClub(),
              ]);
              $crud->save();
       }
        return redirect('/playergroup');
    }
    public function delete($id){
        $playergroup = Playergroup::where('id',$id)->first();
        //return $users; 
      
        $affectedRows = Playergroup::where('id','=',$id);
        if($affectedRows->delete()) {
            $players_list = \App\Player::where('player_group', $id)->get();
            if(!empty($players_list)) {
                foreach($players_list as $player_list) {
                    if(file_exists(public_path('img/player').'/'.$player_list->image)){
                        unlink(public_path('img/player').'/'.$player_list->image);
                    }
                }
            }
            $affected_player_Rows = \App\Player::where('player_group','=',$id);
            $affected_player_Rows->delete();
        }
        return redirect('/playergroup');
    }
}
