<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Team;
use DB;

class TeamController extends Controller
{
public function index() {
        $team           = Team::where('club_id',\App\Club::loginClub())->get();
        $data['teams']  = $team; 
        return view('team.index',$data);
        
    }
    public function setup($id = "") {
        $data = array();
        $data['button']     = "Add Team";
        if($id != "") {
            $team           = Team::where('id',$id)->first();
            $data['team']   = $team;
            $data['button'] = "Update Team";
        }
        return view('team.form',$data);
    }
    public function commit(Request $request) {
        $club_id    = \App\Club::loginClub();
        $id         = $request->input('team_id'); 
        if($id != "") {
             //return $request->all();
             $this->validate($request,[
                'name'      =>'required',
             ]);
             $user = Team::find($id);
             $user->name         = $request->input('name');    
             $user->club_id      = $club_id;    
             $user->save();

        } else {

             $this->validate($request,[
                'name'          =>'required',
             ]);

              $crud = new Team([
                 'name'          => $request->input('name'),
                 'club_id'       => $club_id
               ]);

               $crud->save();
        }
         return redirect('/team');
    }
    public function delete($id){
        $affectedRows = Team::where('id','=',$id);
        if($affectedRows->delete()) { //$affectedRows->delete()
           $affected_player_group = \App\Playergroup::where('team_id',$id)->groupBy('team_id')->selectRaw('GROUP_CONCAT(id) as ids')->first();
           if(isset($affected_player_group->ids) && $affected_player_group->ids != "" ) {
                $players_list = \App\Player::whereIn('player_group', explode(",", $affected_player_group->ids))->get();
                   foreach($players_list as $player_list) {
                        if(file_exists(public_path('img/player').'/'.$player_list->image)){
                            unlink(public_path('img/player').'/'.$player_list->image);
                        }  
                        $players_list_delete  = \App\Player::where('id','=',$player_list->id)->delete();
                   }
           }
            $affectedPlayGroundRows   = \App\Playergroup::where('team_id','=',$id);
           $affectedPlayGroundRows->delete();
        }
        return redirect('/team');
    }
}
