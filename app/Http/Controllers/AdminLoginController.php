<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class AdminLoginController extends Controller
{
    protected $guard = 'admin';
    protected $redirectTo = '/admin';

    public function __construct() {
      //$this->middleware('guest:admin', ['except' => ['logout']]);
    }
    public function showLoginForm() {
      return view('admin.login');
    }
    public function login(Request $request) {
      //return $request->all();
      // Validate the form data
      $this->validate($request, [
        'username'      => 'required',
        'password'      => 'required|min:6'
      ]);
      
      if (Auth::guard('admin')->attempt(['email' => $request->username, 'password' => $request->password,'role' => 1], $request->remember)) {
        return redirect('/admin/home');
      }
      return back()->withErrors(['loginerror' => 'Username and Password are wrong.']);
    }
    public function logout() {
        Auth::guard('admin')->logout();
        return redirect('/admin');
    }
    
}
