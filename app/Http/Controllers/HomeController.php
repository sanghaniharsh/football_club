<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Club;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $user       = auth()->guard('web')->user();
        $user_id    = $user['id'];
        $clubs      = Club::leftJoin('admins', 'admins.club', '=', 'clubs.id')
                        ->select('clubs.*')
                        ->where('admins.id','=',$user_id)
                        ->get();
        $data['clubs'] = $clubs ;
        
        return view('user.home',$data);
    }
    public function setup() {
        $data       = array();
        $club_id    = \App\Club::loginClub();
        if($club_id != "") {
            $club           = Club::where('id',$club_id)->first();
            $data['club']  = $club;
        }
        return view('user.form',$data);
    }
    public function commit(Request $request) {
        $id = $request->input('club_id'); 
       if($id != "") {
            //return $request->all();
            $this->validate($request,[
               'club_name'      =>'required',
               'discription'    =>'required',
            ]);
            $user = Club::find($id);
            $user->name         = $request->input('club_name');     
            $user->discription  = $request->input('discription');
           
            if($request->hasfile('club_image'))
               {
                  $users = Club::where('id',$id)->first();
                  if(file_exists(public_path('img/admin').'/'.$users->image)){
                        unlink(public_path('img/admin').'/'.$users->image);
                   }
                  $images   = $request->file('club_image');
                  $names    = time().$images->getClientOriginalName();
                  $images->move(public_path().'/img/club/', $names);  
                  $user->image    = $names;
               }
            $user->save();
            
       } else {
            $this->validate($request,[
               'club_name'          =>'required',
               'discription'        =>'required',
               'club_image'         =>'required|image|mimes:jpeg,jpg',
            ]);

            $names = "";
            if($request->hasfile('club_image'))
               {
                  $images = $request->file('club_image');
                  $names = time().$images->getClientOriginalName();
                  $images->move(public_path().'/img/club/', $names);   
               }

             $crud = new Club([
                'name'          => $request->input('club_name'),
                'discription'   => $request->input('discription'),
                'status'        => "1",
                'image'         => $names,
              ]);
             
              $crud->save();
       }
        return redirect('/setup/'.$id);
    }
}
