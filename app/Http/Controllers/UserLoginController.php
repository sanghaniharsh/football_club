<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class UserLoginController extends Controller
{
    public function showLoginForm() {
      return view('user.login');
    }
    public function login(Request $request) {
       // return $request->all();
      // Validate the form data
      $this->validate($request, [
        'username'      => 'required',
        'password'      => 'required|min:6'
      ]);
      
      if (Auth::guard('club_admin')->attempt(['email' => $request->username, 'password' => $request->password,'role'=>'2'], $request->remember)) {
        return redirect('/home');
      }
      return back()->withErrors(['loginerror' => 'Username and Password are wrong.']);
    }
}
