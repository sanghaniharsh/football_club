<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use App\Club;
use DB;

class AdminController extends Controller
{
    public function __construct() {
        $this->middleware('auth:admin');
    }
    public function index() {
        $admins         = Admin::all();
        $data['admins'] = $admins;
        //return $data['admin']; 
        return view('admin.home',$data);
    }
    public function setup($id = "") {
        $data = array();
        //$clubs          = Club::all();
        
        if($id != "") {
            DB::enableQueryLog();
            $admin          = Admin::where('id',$id)->first();
            $clubs          = Club::leftJoin('admins', 'admins.club', '=', 'clubs.id')
                            ->select('admins.id as admin_id','clubs.id','clubs.name')
                            ->where('admins.id','=',$id)
                            ->orWhereNull('admins.id')
                            ->get();
            $query = DB::getQueryLog();
            $data['button']     = "Update User";
            $data['admin']  = $admin;
        } else {
            $clubs          = Club::leftJoin('admins', 'admins.club', '=', 'clubs.id')
                            ->select('admins.id as admin_id','clubs.id','clubs.name')
                            ->whereNull('admins.id')
                            ->get();
            $data['button']     = "Add User";
            
        }
        $data['clubs']  = $clubs;
        return view('admin.form',$data);
    } 
    public function commit(Request $request) {
        //return $request->all();
        $id = $request->input('user_id'); 
      //return $request->all();
       if($id != "") {
            $this->validate($request,[
               'name'           =>'required',
               'username'       =>'required',
               'email'          =>'required|email',
               'gender'         =>'required',
               'role'           =>'required',
            ]);
            $user = Admin::find($id);
            $user->name         = $request->input('name');     
            $user->username     = $request->input('username');     
            $user->email        = $request->input('email');
            $user->role         = $request->input('role');
            $user->club         = $request->input('club');
            $password           = $request->input('password');
            
            if($password != "") {
                $user->password    = bcrypt($request->input('password'));
            }
            if($request->hasfile('profile_image'))
               {
                  $users = Admin::where('id',$id)->first();
                  if(file_exists(public_path('img/admin').'/'.$users->image)){
                        unlink(public_path('img/admin').'/'.$users->image);
                   }
                  $images   = $request->file('profile_image');
                  $names    = time().$images->getClientOriginalName();
                  $images->move(public_path().'/img/admin/', $names);  
                  $user->image    = $names;
               }
            $user->gender       = $request->input('gender');
            $user->save();
            
       } else {
            $this->validate($request,[
               'name'           =>'required',
               'username'       =>'required',
               'email'          =>'required|email',
               'password'       =>'required|max:8',
               'gender'         =>'required',
               'role'           =>'required',
               'profile_image'  =>'required|image|mimes:jpeg,jpg',
            ]);

            $names = "";
            if($request->hasfile('profile_image'))
               {
                  $images = $request->file('profile_image');
                  $names = time().$images->getClientOriginalName();
                  $images->move(public_path().'/img/admin/', $names);   
               }

             $crud = new Admin([
                'name'          => $request->input('name'),
                'username'      => $request->input('username'),
                'email'         => $request->input('email'),
                'password'      => bcrypt($request->input('password')),
                'birthdate'     => $request->input('birthdate'),
                'gender'        => $request->input('gender'),
                'role'          => $request->input('role'),
                'club'          => $request->input('club'),
                'image'         => $names,
              ]);
             
              $crud->save();
       }
        return redirect('/admin/home');
    }
    public function delete($id){
        $users = Admin::where('id',$id)->first();
        //return $users; 
        if(file_exists(public_path('img/admin').'/'.$users->image)){
              unlink(public_path('img/admin').'/'.$users->image);
         }
        $affectedRows = Admin::where('id','=',$id);
        $affectedRows->delete();
        return redirect('/admin/home');
    }
    
}
