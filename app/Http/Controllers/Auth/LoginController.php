<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Club;

//use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class LoginController extends Controller
{
    protected $guard = 'web';
    protected $redirectTo = '/home';
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    //use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest')->except('logout');
    }
     public function showLoginForm() {
      return view('user.login');
    }
    public function login(Request $request) {
        //return $request->all();
      // Validate the form data
      $this->validate($request, [
        'username'      => 'required',
        'password'      => 'required|min:6'
      ]);
      
      if (Auth::guard('web')->attempt(['email' => $request->username, 'password' => $request->password,'role' => 2], $request->remember)) {           
         
        return redirect('/setup');
      }
      return back()->withErrors(['loginerror' => 'Username and Password are wrong.']);
    }
    public function logout() {
        Auth::guard('web')->logout();
        return redirect('/');
    }
}
