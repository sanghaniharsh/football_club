<?php

namespace App\Http\Controllers;
use App\Club;
use DB;
use Illuminate\Http\Request;

class ClubController extends Controller
{
    public function __construct() {
        $this->middleware('auth:admin');
    }
    public function index() {
        $clubs          = Club::all();
        $data['clubs'] = $clubs ; 
        return view('admin.club.index',$data);
    }
    public function setup($id = "") {
        $data = array();
        $data['button']     = "Add Club";
        if($id != "") {
            $club           = Club::where('id',$id)->first();
            $data['club']   = $club;
            $data['button']       = "Update Club";
        }
        return view('admin.club.form',$data);
    }
    public function commit(Request $request) {
        $id = $request->input('club_id'); 
       if($id != "") {
            //return $request->all();
            $this->validate($request,[
               'club_name'      =>'required',
               'discription'    =>'required',
            ]);
            $user = Club::find($id);
            $user->name         = $request->input('club_name');     
            $user->discription  = $request->input('discription');
           
            if($request->hasfile('club_image'))
               {
                  $users = Club::where('id',$id)->first();
                  if(file_exists(public_path('img/admin').'/'.$users->image)){
                        unlink(public_path('img/admin').'/'.$users->image);
                   }
                  $images   = $request->file('club_image');
                  $names    = time().$images->getClientOriginalName();
                  $images->move(public_path().'/img/club/', $names);  
                  $user->image    = $names;
               }
            $user->save();
            
       } else {
            $this->validate($request,[
               'club_name'          =>'required',
               'discription'        =>'required',
               'club_image'         =>'required|image|mimes:jpeg,jpg',
            ]);

            $names = "";
            if($request->hasfile('club_image'))
               {
                  $images = $request->file('club_image');
                  $names = time().$images->getClientOriginalName();
                  $images->move(public_path().'/img/club/', $names);   
               }

             $crud = new Club([
                'name'          => $request->input('club_name'),
                'discription'   => $request->input('discription'),
                'status'        => "1",
                'image'         => $names,
              ]);
             
              $crud->save();
       }
        return redirect('/admin/club');
    }
    public function delete($id){
        $club = Club::where('id',$id)->first();
        //return $users; 
        if(file_exists(public_path('img/club').'/'.$club->image)){
              unlink(public_path('img/club').'/'.$club->image);
         }
        $affectedRows = Club::where('id','=',$id);
        $affectedRows->delete();
        return redirect('/admin/club');
    }
}
