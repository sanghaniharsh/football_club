<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    protected $table = 'player';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
          'name','image','club_id','player_group','status'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
