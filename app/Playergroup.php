<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Playergroup extends Model
{
    protected $table = 'player_group';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
          'name', 'team_id','club_id'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
