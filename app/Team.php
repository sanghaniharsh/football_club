<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = 'team';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
          'name','club_id'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
