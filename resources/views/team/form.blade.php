<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<style>
    .error {
        color: red;
    }
</style>
@php $url = '/team_commit' @endphp
@if(isset($team->id)) 
   @php $url = '/team_update'  @endphp
@endif
@include('user.header')
<div class="container">
    <div class='row'>
        <div class='col-md-12'>
               <h2>Team Register</h2>
                <!-- page start-->
                {!! Form::open(['id'=>'user_form','url' => $url,'enctype'=>'multipart/form-data']) !!}
                <input type="hidden" name="team_id" value="{{ (isset($team->id))? $team->id : ''}}">
                <div class="form-group">
                    @php $team_name = (isset($team->name))? $team->name : "" @endphp
                    {!! Form::label('club_name', 'Team Name') !!}
                    {!! Form::text('name', $team_name , ['class' => 'form-control']) !!}
                    <span class="error"><?php echo $errors->first('name'); ?></span>
                </div>
                
                 <br><br>
                {!! Form::submit($button,['class' => 'btn btn-success']) !!}
                {!! Form::close() !!}
                <!-- page end-->
        </div>
    </div>
</div>