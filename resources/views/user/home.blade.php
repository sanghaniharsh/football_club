<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link href="{{url('public/css/jquery.dataTables.min.css')}}" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="{{url('public/js/jquery.dataTables.min.js')}}"></script>
<style>
    .add_club {
        float: right;
    }
</style>
@include('user.header');
<div class="container">
    <div class='row'>
        <div class='col-md-12'>
                <h2>Club List</h2>
                
                <section id="main-content">
                    <section class="wrapper site-min-height">
                        <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="usertable">
                            <thead>
                                <tr>
                                    <th>Club Name</th>
                                    <th>Image</th>
                                    <th>Manage</th>
                                </tr>
                            <thead>
                                @foreach($clubs as $club)
                                 @php 
                                    $img    = "avatar.png";
                                    if($club['image'] != "") {
                                        if(file_exists(public_path('img/club').'/'.$club['image'])){
                                            $img = $club['image'];
                                        }
                                    }
                                 @endphp
                                <tr>
                                    <td>{{$club['name']}}</td>
                                    <td>{{ Html::image(url('public/img/club').'/'.$img, 'alt text', array('class' => 'css-class','width' => '100px','height' => '100px')) }}</td>
                                    <td><a href="{{url('/edit').'/'.$club['id']}}" class="btn btn-primary">Edit</a><a href="{{ url('/delete').'/'.$club['id']}}" class="btn btn-danger">Delete</a></td>
                                </tr>
                                @endforeach
                            <tbody>
                                
                            <tbody>
                        </table>
                    </section>
                </section>
        </div>
    </div>
</div>
<script>
    $(document).ready( function () {
        $('#usertable').DataTable();
    } );
</script>