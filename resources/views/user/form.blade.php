<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<style>
    .error {
        color: red;
    }
</style>
@php $url = '/commit' @endphp
@if(isset($club->id)) 
   @php $url = '/update'  @endphp
@endif
@include('user.header')
<div class="container">
    <div class='row'>
        <div class='col-md-12'>
                <!-- page start-->
                {!! Form::open(['id'=>'user_form','url' => $url,'enctype'=>'multipart/form-data']) !!}
                <input type="hidden" name="club_id" value="{{ (isset($club->id))? $club->id : ''}}">
                <div class="form-group">
                    @php $club_name = (isset($club->name))? $club->name : "" @endphp
                    {!! Form::label('club_name', 'Club Name') !!}
                    {!! Form::text('club_name', $club_name , ['class' => 'form-control']) !!}
                    <span class="error"><?php echo $errors->first('club_name'); ?></span>
                </div>
                <div class="form-group">
                    @php $discription = (isset($club->discription))? $club->discription : "" @endphp
                    {!! Form::label('discription', 'Discription') !!}
                    {{ Form::textarea('discription',$discription, array('class' =>'form-control input', 'cols' => 20, 'rows' =>5, 'required' => ''))}}
                    <span class="error"><?php echo $errors->first('discription'); ?></span>
                </div> 
                <div class="form-group">
                    {!! Form::label('image', 'Club Image') !!}
                    <?php echo Form::file('club_image',['class' => 'form-control']) ?>
                    <span class="error"><?php echo $errors->first('club_image'); ?></span>
                </div>
                 @if(isset($club->image) && $club->image != "")
                    @if(file_exists(public_path('img/club').'/'.$club->image))
                         {{ Html::image(url('public/img/club').'/'.$club->image, 'alt text', array('class' => 'css-class','width' => '100px','height' => '100px')) }}
                    @endif
                 @endif
                 <br><br>
                {!! Form::submit('Update Club!',['class' => 'btn btn-success']) !!}
                {!! Form::close() !!}
                <!-- page end-->
        </div>
    </div>
</div>