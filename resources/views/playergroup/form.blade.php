<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<style>
    .error {
        color: red;
    }
</style>
@php $url = '/playergroup_commit' @endphp
@if(isset($playergroup->id)) 
   @php $url = '/playergroup_update'  @endphp
@endif
@include('user.header')
<div class="container">
    <div class='row'>
        <div class='col-md-12'>
               <h2>Playergroup Form</h2>
                <!-- page start-->
                {!! Form::open(['id'=>'playergroup_form','url' => $url,'enctype'=>'multipart/form-data']) !!}
                <input type="hidden" name="playergroup_id" value="{{ (isset($playergroup->id))? $playergroup->id : ''}}">
                <div class="form-group">
                    @php $name = (isset($playergroup->name))? $playergroup->name : "" @endphp
                    {!! Form::label('playergroup_name', 'Playergroup Name') !!}
                    {!! Form::text('name', $name , ['class' => 'form-control']) !!}
                    <span class="error"><?php echo $errors->first('name'); ?></span>
                </div>
                 @php
                    $team_value[''] = "-Select Team-";
                    foreach ($teams as $team) {
                        $team_value[$team->id] = $team->name;
                    }
                @endphp
                <div class="form-group">
                    @php $team_id = (isset($playergroup->team_id))? $playergroup->team_id : "" @endphp
                    {!! Form::label('team_id', 'Team') !!}
                    {!! Form::select('team_id',$team_value,$team_id,['class' => 'form-control']) !!}
                    <span class="error"><?php echo $errors->first('team_id'); ?></span>
                </div>
                
                
                
                 <br><br>
                {!! Form::submit($button,['class' => 'btn btn-success']) !!}
                {!! Form::close() !!}
                <!-- page end-->
        </div>
    </div>
</div>