<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link href="{{url('public/css/jquery.dataTables.min.css')}}" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="{{url('public/js/jquery.dataTables.min.js')}}"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<style>
    .add_club {
        float: right;
    }
</style>
@include('user.header')
<div class="container">
    <div class='row'>
        <div class='col-md-12'>
            
                <h2>Playergroup List</h2>
                <a class="btn btn-primary mb-4 add_club" href="{{ url('/playergroup_setup')}}">Add Playergroup</a>
                <section id="main-content">
                    <section class="wrapper site-min-height">
                        <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="usertable">
                            <thead>
                                <tr>
                                    <th>Playergroup Name</th>
                                    <th>Team Name</th>
                                    <th>Manage</th>
                                </tr>
                            <thead>
                            <tbody>
                                @foreach($playergroup as $playergroup)
                                 @php 
                                    $img    = "avatar.png";
                                    if($playergroup['image'] != "") {
                                        if(file_exists(public_path('img/playergroup').'/'.$playergroup['image'])){
                                            $img = $playergroup['image'];
                                        }
                                    }
                                 @endphp
                                <tr>
                                    <td>{{$playergroup['name']}}</td>
                                    <td>{{$playergroup['team_name']}}</td>
                                    <td><a href="{{url('/playergroup_edit').'/'.$playergroup['id']}}" class="btn btn-primary">Edit</a><a href="{{ url('/playergroup_delete').'/'.$playergroup['id']}}" onclick="return confirm('Are you sure? ');" class="btn btn-danger">Delete</a></td>
                                </tr>
                                @endforeach
                            <tbody>
                        </table>
                    </section>
                </section>
        </div>
    </div>
</div>
<script>
    $(document).ready( function () {
        $('#usertable').DataTable();
    } );
</script>