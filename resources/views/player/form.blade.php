<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<style>
    .error {
        color: red;
    }
</style>
@php $url = '/player_commit' @endphp
@if(isset($player->id)) 
   @php $url = '/player_update'  @endphp
@endif
@include('user.header')
<div class="container">
    <div class='row'>
        <div class='col-md-12'>
               <h2>Player Form</h2>
                <!-- page start-->
                {!! Form::open(['id'=>'player_form','url' => $url,'enctype'=>'multipart/form-data']) !!}
                <input type="hidden" name="player_id" value="{{ (isset($player->id))? $player->id : ''}}">
                <div class="form-group">
                    @php $player_name = (isset($player->name))? $player->name : "" @endphp
                    {!! Form::label('player_name', 'Player Name') !!}
                    {!! Form::text('player_name', $player_name , ['class' => 'form-control']) !!}
                    <span class="error"><?php echo $errors->first('player_name'); ?></span>
                </div>
                @php
                    $playgroup_value[''] = "-Select Player Group-";
                    foreach ($player_groups as $player_group) {
                        $playgroup_value[$player_group->id] = $player_group->name;
                    }
                @endphp
                <div class="form-group club_list">
                    @php $play_group_value = (isset($player->player_group))? $player->player_group : "" @endphp
                    {!! Form::label('player_group', 'Player Group') !!}
                    {!! Form::select('player_group',$playgroup_value,$play_group_value,['class' => 'form-control']) !!}
                    <span class="error"><?php echo $errors->first('player_group'); ?></span>
                </div>
                <div class="form-group">
                    {!! Form::label('image', 'Player Image') !!}
                    <?php echo Form::file('player_image',['class' => 'form-control']) ?>
                    <span class="error"><?php echo $errors->first('player_image'); ?></span>
                </div>
                 @if(isset($player->image) && $player->image != "")
                    @if(file_exists(public_path('img/player').'/'.$player->image))
                         {{ Html::image(url('public/img/player').'/'.$player->image, 'alt text', array('class' => 'css-class','width' => '100px','height' => '100px')) }}
                    @endif
                 @endif
                 <br><br>
                {!! Form::submit($button,['class' => 'btn btn-success']) !!}
                {!! Form::close() !!}
                <!-- page end-->
        </div>
    </div>
</div>