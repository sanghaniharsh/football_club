<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<style>
    .error {
        color: red;
    }
</style>
@php $url = '/admin/commit' @endphp
@if(isset($admin->id)) 
   @php $url = '/admin/update'  @endphp
@endif
@include('admin.header')
<div class="container">
    <div class='row'>
        <div class='col-md-12'>
               <h2>Register Form</h2>
                <!-- page start-->
                {!! Form::open(['id'=>'user_form','url' => $url,'enctype'=>'multipart/form-data']) !!}
                <input type="hidden" name="user_id" value="{{ (isset($admin->id))? $admin->id : ''}}">
                <div class="form-group">
                    @php $name = (isset($admin->name))? $admin->name : "" @endphp
                    {!! Form::label('name', 'Name') !!}
                    {!! Form::text('name', $name , ['class' => 'form-control']) !!}
                    <span class="error"><?php echo $errors->first('name'); ?></span>
                </div>
                <div class="form-group">
                    @php $username = (isset($admin->username))? $admin->username : "" @endphp
                    {!! Form::label('username', 'Username') !!}
                    {!! Form::text('username', $username , ['class' => 'form-control']) !!}
                    <span class="error"><?php echo $errors->first('username'); ?></span>
                </div>
                <div class="form-group">
                    @php $email = (isset($admin->email))? $admin->email : "" @endphp
                    {!! Form::label('email', 'Email') !!}
                    {!! Form::text('email',$email, ['class' => 'form-control','autocomplete' => 'no']) !!}
                    <span class="error"><?php echo $errors->first('email'); ?></span>
                </div>
                <div class="form-group">
                    {!! Form::label('password', 'Password') !!}
                    {!! Form::password('password',['class' => 'form-control']) !!}
                    <span class="error"><?php echo $errors->first('password'); ?></span>
                </div>
                @php 
                    $genders = array('male'=>'Male','female'=>'Female');
                @endphp
                <div class="form-group">
                    @php $gender = (isset($admin->gender))? $admin->gender : "" @endphp
                    {!! Form::label('gender', 'Gender') !!}
                    {!! Form::select('gender',$genders,$gender,['class' => 'form-control']) !!}
                    <span class="error"><?php echo $errors->first('gender'); ?></span>
                </div>
                 @php 
                    $roles = array('1'=>'Admin','2'=>'Club Admin');
                @endphp
                <div class="form-group">
                    @php $role = (isset($admin->role))? $admin->role : "" @endphp
                    {!! Form::label('role', 'Role') !!}
                    {!! Form::select('role',$roles,$role,['class' => 'form-control role']) !!}
                    <span class="error"><?php echo $errors->first('role'); ?></span>
                </div>
                @php
                    $club_value[] = "-Select Club-";
                    foreach ($clubs as $club) {
                        $club_value[$club->id] = $club->name;
                    }
                @endphp
                <div class="form-group club_list" @if(isset($role) && $role == "2") style="display: block;"  @else  style="display: none;" @endif >
                    @php $club = (isset($admin->club))? $admin->club : "" @endphp
                    {!! Form::label('club', 'Club') !!}
                    {!! Form::select('club',$club_value,$club,['class' => 'form-control']) !!}
                    <span class="error"><?php echo $errors->first('role'); ?></span>
                </div>
                <div class="form-group">
                    {!! Form::label('image', 'Profile Image') !!}
                    <?php echo Form::file('profile_image',['class' => 'form-control']) ?>
                    <span class="error"><?php echo $errors->first('profile_image'); ?></span>
                </div>
                 @if(isset($admin->image) && $admin->image != "")
                    @if(file_exists(public_path('img/admin').'/'.$admin->image))
                         {{ Html::image(url('public/img/admin').'/'.$admin->image, 'alt text', array('class' => 'css-class','width' => '100px','height' => '100px')) }}
                    @endif
                 @endif
                 <br><br>
                {!! Form::submit($button,['class' => 'btn btn-success']) !!}
                {!! Form::close() !!}
                <!-- page end-->
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
       $('.role').on('change',function(){
          if($(this).val() == "2") {
              $('.club_list').css('display','block');
          } else {
              $('.club_list').css('display','none');
          }
       }); 
    });
</script>