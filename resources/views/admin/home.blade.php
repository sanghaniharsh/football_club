<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link href="{{url('public/css/jquery.dataTables.min.css')}}" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="{{url('public/js/jquery.dataTables.min.js')}}"></script>
<style>
    .add_user {
        float: right;
    }
</style>
@include('admin.header')
<div class="container">
    <div class='row'>
        <div class='col-md-12'>
                <h2>Club List</h2>
                <a class="btn btn-primary mb-4 add_user" href="{{ url('/admin/setup')}}">Add User</a>
                <section id="main-content">
                    <section class="wrapper site-min-height">
                        <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="usertable">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Image</th>
                                    <th>Manage</th>
                                </tr>
                            <thead>
                            <tbody>
                                @foreach($admins as $admin)
                                 @php 
                                    $img    = "avatar.png";
                                    if($admin['image'] != "") {
                                        if(file_exists(public_path('img/admin').'/'.$admin->image)){
                                            $img = $admin['image'];
                                        }
                                    }
                                 @endphp
                                <tr>
                                    <td>{{$admin['name']}}</td>
                                    <td>{{$admin['email']}}</td>
                                    <td>{{ Html::image(url('public/img/admin').'/'.$img, 'alt text', array('class' => 'css-class','width' => '100px','height' => '100px')) }}</td>
                                    <td><a href="{{url('/admin/edit').'/'.$admin['id']}}" class="btn btn-primary">Edit</a><a href="{{ url('/admin/delete').'/'.$admin['id']}}" class="btn btn-danger" onclick="return confirm('Are you sure? ');">Delete</a></td>
                                </tr>
                                @endforeach
                            <tbody>
                        </table>
                    </section>
                </section>
        </div>
    </div>
</div>
<script>
    $(document).ready( function () {
        $('#usertable').DataTable();
    } );
</script>