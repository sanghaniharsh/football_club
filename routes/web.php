<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admin','AdminLoginController@showLoginForm');
Route::post('/admin/login','AdminLoginController@login');
Route::get('/admin/logout', 'AdminLoginController@logout');
Route::get('/admin/home','AdminController@index');
Route::get('/admin/setup','AdminController@setup');
Route::get('/admin/setup','AdminController@setup');
Route::get('/admin/edit/{id}','AdminController@setup');
Route::get('/admin/delete/{id}','AdminController@delete');
Route::post('/admin/commit','AdminController@commit');
Route::post('/admin/update','AdminController@commit');


Route::get('/admin/club','ClubController@index');
Route::get('/admin/club_setup','ClubController@setup');
Route::post('/admin/club_commit','ClubController@commit');
Route::get('/admin/club_delete/{id}','ClubController@delete');
Route::get('/admin/club_edit/{id}','ClubController@setup');
Route::post('/admin/club_update','ClubController@commit');

Route::get('/player','PlayerController@index');
Route::get('/player_setup','PlayerController@setup');
Route::post('/player_commit','PlayerController@commit');
Route::get('/player_delete/{id}','PlayerController@delete');
Route::get('/player_edit/{id}','PlayerController@setup');
Route::post('/player_update','PlayerController@commit');


Route::get('/playergroup','PlayergroupController@index');
Route::get('/playergroup_setup','PlayergroupController@setup');
Route::post('/playergroup_commit','PlayergroupController@commit');
Route::get('/playergroup_delete/{id}','PlayergroupController@delete');
Route::get('/playergroup_edit/{id}','PlayergroupController@setup');
Route::post('/playergroup_update','PlayergroupController@commit');


Route::get('/','Auth\LoginController@showLoginForm');
Route::get('/logout','Auth\LoginController@logout');
Route::get('/setup','HomeController@setup');
Route::get('/commit','HomeController@commit');
Route::get('/edit/{id}','HomeController@setup');
Route::post('/update','HomeController@commit');

Route::get('/team','TeamController@index');
Route::get('/team_setup','TeamController@setup');
Route::get('/team_edit/{id}','TeamController@setup');
Route::post('/team_commit','TeamController@commit');
Route::post('/team_update','TeamController@commit');
Route::get('/team_delete/{id}','TeamController@delete');
//Route::post('/login','UserLoginController@login');
//Route::get('/home','UserController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
