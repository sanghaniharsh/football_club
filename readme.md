This is basic demo of the football club application. Please use the football_club.sql SQL file from the directory.

You can access the admin panel at here : BASE_URL/admin

Login details for admin :

Email 		: admin@gmail.com
Password 	: 123456

You can create New admin and Club admin from the admin panel.

*********************************************************************************

You can access club admin panel at here : BASE_URL/ 

Login details for Club admin :

Email 		: club@gmail.com
Password 	: 123456

You can create Team, Player Group & Player of the club from the club admin.

*********************************************************************************

Pending :

1) I did not list out all the Teams, Player Group & player in admin panel